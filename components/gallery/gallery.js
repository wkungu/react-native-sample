import React, { Component } from 'react'
import { View, CameraRoll, ScrollView, Image, StyleSheet, Text } from 'react-native'
import { Link } from 'react-router-native'
import { Card, CardItem, Button, URL } from '../common'


const styles = StyleSheet.create({
    button: {
        flex: 1,
        alignSelf: 'stretch',
        backgroundColor: '#fff',
        borderRadius: 3,
        borderWidth: 1,
        borderColor: '#007aff',
        marginLeft: 5,
        marginRight: 5
    },

    text: {
        alignSelf: 'center',
        color: '#007aff',
        fontSize: 16,
        fontWeight: '600',
        paddingTop: 10,
        paddingBottom: 10
    }

})

export default class gallery extends Component {

    state = {
        photos: []
    }

    _handleButtonPress = () => {
        CameraRoll.getPhotos({
            first: 20,
            assetType: 'All',
        })
            .then(r => {
                this.setState({ photos: r.edges });
            })
            .catch((err) => {
                //Error Loading Images
            });
    };
    render() {

        return (

            <Card animate="slideInLeft">

                <CardItem>
                    <Button onClick={this._handleButtonPress}  >Load Images</Button>
                    <URL to="/camera">Take Photo</URL>

                </CardItem>

                <ScrollView>
                    {this.state.photos.map((p, i) => {
                        return (
                            <Image
                                key={i}
                                style={{
                                    width: '100%',
                                    height: 200,
                                }}
                                source={{ uri: p.node.image.uri }}
                            />
                        );
                    })}
                </ScrollView>

            </Card>

        );

    }
}