import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { Link } from 'react-router-native'
import { observer, inject } from 'mobx-react';
import { Store } from '../services';

const styles = StyleSheet.create({
    header: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 40,
        paddingTop: 5,
        paddingBottom: 5,
        backgroundColor: '#f8f8f8',
        shadowColor: '#000',
        shadowOffset: { height: 0, width: 2 },
        shadowOpacity: 0.9,
        elevation: 2,
        position: 'relative'
    },
    title: {
        fontSize: 14
    },
    nav: {
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    navItem: {
        flex: 1,
        alignItems: 'center',
        padding: 10,
    },
})

@inject('properties') @observer
export class Header extends Component {

    onLogout = () => {
        Store.logout();
    }

    renderLogin = () => {

        let { properties } = this.props;

        switch (properties.logged_in) {

            case true:
                return (
                    <TouchableOpacity style={styles.navItem} onPress={() => this.onLogout()} >
                        <Text>Logout</Text>
                    </TouchableOpacity>
                )
            case false:
                return (
                    <Link
                        to="/login"
                        underlayColor='#f0f4f7'
                        style={styles.navItem} >
                        <Text>Login</Text>
                    </Link>
                )
            default:
                return null;

        }

    }


    renderAlbums = () => {

        let { properties } = this.props;

        switch (properties.logged_in) {

            case true:
                return (
                    <Link
                        to="/albums"
                        underlayColor='#f0f4f7'
                        style={styles.navItem}>
                        <Text>Albums</Text>
                    </Link>
                )
            default:
                return null;

        }

    }


    renderRegister = () => {

        let { properties } = this.props;

        switch (properties.logged_in) {

            case false:
                return (
                    <Link
                        to="/register"
                        underlayColor='#f0f4f7'
                        style={styles.navItem} >
                        <Text>Register</Text>
                    </Link>
                )
            default:
                return null;

        }

    }


    render() {


        return (
            <View style={styles.header}>

                <View style={styles.nav}>

                    <Link
                        to="/"
                        underlayColor='#f0f4f7'
                        style={styles.navItem}>
                        <Text>Home</Text>
                    </Link>

                    <Link
                        to="/gallery"
                        underlayColor='#f0f4f7'
                        style={styles.navItem}>
                        <Text>Gallery</Text>
                    </Link>

                    {this.renderAlbums()}

                    {this.renderRegister()}

                    {this.renderLogin()}


                </View>

            </View>
        )
    }

}



