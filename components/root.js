import React, { Component } from 'react';
import { NativeRouter as Router, Route } from 'react-router-native';
import { View } from 'react-native';
import { Provider } from 'mobx-react';
import { Header } from './common';
import Home from './home/home';
import AlbumList from './music/album-list';
import Register from './registration/register';
import Gallery from './gallery/gallery';
import Camera from './camera/screen';
import Login from './login/login';
import properties from './properties';

const stores = { properties };

export default class Root extends Component {

  render() {
    return (
      <Provider {...stores}>
        <Router>
          <View >
            <Header />
            <Route exact path="/" component={Home} />
            <Route path="/albums" component={AlbumList} />
            <Route path="/login" component={Login} />
            <Route path="/gallery" component={Gallery} />
            <Route path="/camera" component={Camera} />
            <Route path="/register" component={Register} />
          </View>
        </Router>
      </Provider>
    );
  }
}
