import React from 'react'
import {View, Text, Image, TouchableOpacity, StyleSheet, Linking} from 'react-native'
import {Card, CardItem, Button} from '../common'

const styles = StyleSheet.create({
    textarea: {
        flexDirection: 'column',
        justifyContent: 'space-around'
    },

    textHeader: {
        fontSize: 15
    },


    thumbnail: {
        width: 50,
        height: 50,
    },

    image: {
        width: null,
        flex:1,
        height:300
    },

    thumbnailContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 10,
        marginRight: 10
    }
})

const album = ({album}) =>{

    const {title, artist, thumbnail_image, image, url} = album;

    return(
        <Card>
            
            <CardItem>

                <View style={styles.thumbnailContainer}>
                    <Image style={styles.thumbnail} source={{uri: thumbnail_image}} />
                </View>

                <View style={styles.textarea}>
                    <Text style={styles.textHeader}>{title}</Text>
                    <Text>{artist}</Text>
                </View>

            </CardItem>

            <CardItem>
                <Image style={styles.image} source={{uri: image}} />
            </CardItem>

            <CardItem>
                <Button onClick={() => Linking.openURL(url)} >Buy Now!</Button>
            </CardItem>

        </Card>
    )
}

export default album;