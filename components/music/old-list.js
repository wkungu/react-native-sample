import React, { Component } from 'react';
import { View, Text, ListView } from 'react-native';
import { Redirect } from 'react-router-native';
import { observer, inject } from 'mobx-react';
import axios from 'axios';
import Album from './album-detail';
import { Spinner } from '../common';

@inject('properties') @observer
export default class AlbumList extends Component {

    constructor() {

        super()

        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })

        this.state = {
            albums: ds.cloneWithRows([])
        }

    }

    componentDidMount = () => {
        axios.get('https://rallycoding.herokuapp.com/api/music_albums')
            .then(response => this.setState({ albums: this.state.albums.cloneWithRows(response.data) }))
    }

    renderContent = (albums) => {
        return (
            <ListView
                dataSource={albums}
                renderRow={(data) => <Album album={data} />}
            />
        )
    }

    render() {

        let { properties } = this.props;

        return (
            <View>
                {(!properties.logged_in) ? <Redirect to="/login" /> : null}
                {(this.state.albums.getRowCount() === 0) ? <Spinner size='large' /> : this.renderContent(this.state.albums)}

            </View>
        )
    }

}
