import React, { Component } from 'react';
import { View, Text, FlatList } from 'react-native';
import { Redirect } from 'react-router-native';
import { observer, inject } from 'mobx-react';
import axios from 'axios';
import Album from './album-detail';
import { Spinner } from '../common';
import uuidv1 from 'uuid/v1';

@inject('properties') @observer
export default class AlbumList extends Component {

    constructor() {

        super()

        this.state = {
            albums: []
        }

    }

    _keyExtractor = (item, index) => uuidv1(); //Generates unique keys 

    componentDidMount = () => {
        axios.get('https://rallycoding.herokuapp.com/api/music_albums')
            .then(response => this.setState({ albums: response.data }))
    }

    renderContent = (albums) => {
        return (
            <FlatList
                data={albums}
                keyExtractor={this._keyExtractor}
                renderItem={({ item }) => <Album album={item} />}
            />
        )
    }

    render() {

        let { properties } = this.props;

        return (
            <View>
                {(!properties.logged_in) ? <Redirect to="/login" /> : null}
                {(this.state.albums.length === 0) ? <Spinner size='large' /> : this.renderContent(this.state.albums)}

            </View>
        )
    }

}
