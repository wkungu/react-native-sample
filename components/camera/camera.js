import React, { Component } from 'react'
import { View, Text, StyleSheet, Dimensions, Camera } from 'react-native'
import { RNCamera } from 'react-native-camera'
import { Card, CardItem } from '../common'


export default class camera extends Component {

    takePicture() {
        this.camera.capture()
            .then((data) => console.log(data))
            .catch(err => console.error(err));
    }

    render() {

        const styles = StyleSheet.create({
            preview: {
                flex: 1,
                justifyContent: 'flex-end',
                alignItems: 'center',
                height: Dimensions.get('window').height,
                width: Dimensions.get('window').width
            },
            capture: {
                flex: 0,
                backgroundColor: '#fff',
                borderRadius: 5,
                color: '#000',
                padding: 10,
                margin: 40
            }
        })


        return (

            <Card animate="slideInLeft">

                <RNCamera
                    ref={(cam) => {
                        this.camera = cam;
                    }}
                    style={styles.preview}
                    aspect={RNCamera.constants.Aspect.fill}>
                    <Text style={styles.capture} onPress={this.takePicture.bind(this)}>
                        [CAPTURE]
                    </Text>
                </RNCamera>

            </Card>

        );

    }
}