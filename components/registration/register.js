import React, { Component } from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { observer, inject } from 'mobx-react'
import { Store } from "../services"
import wrapper from './wrapper'
import { Card, CardItem, Input, Button, Spinner } from '../common'

const styles = StyleSheet.create({
	title: {
		justifyContent: 'center',
		alignItems: 'center',
		color: 'skyblue',
		fontWeight: '600',
		textAlign: 'center',
		textDecorationLine: 'underline',
		height: 40,
		paddingLeft: 5,
		paddingTop: 5,
		paddingBottom: 5,
	}
})

@inject('properties') @observer
export default class Register extends Component {

	constructor(props) {
		super(props)

		this.state = {
			email: '',
			emailError: '',
			password: '',
			passwordError: '',
			disabled: true
		}
	}

	emailValid = () => {

		this.setState({
			emailError: wrapper('email', this.state.email)
		})

	}

	passwordValid = () => {

		this.setState({
			passwordError: wrapper('password', this.state.password)
		}, () => {
			if (!this.state.emailError && !this.state.passwordError) {
				this.setState({ disabled: false });
			}
		})

	}

	submitForm = () => {

		const { properties } = this.props;

		properties.showSpinner(true);

		let values = {
			user_email: this.state.email,
			user_password: this.state.password
		}

		Store.registerUser(values).then((user) => {

			properties.showSpinner(false);

		}).catch((error) => {

			let err = {
				status: true,
				title: 'Login failed: - ',
				message: error.message
			}

			properties.setError(err);
			properties.showSpinner(false);
		});

	}



	register = () => {

		const emailError = wrapper('email', this.state.email)
		const passwordError = wrapper('password', this.state.password)

		this.setState({
			emailError: emailError,
			passwordError: passwordError
		});

		if (!emailError && !passwordError) {
			this.submitForm();
		}
	}

	renderForm = () => {

		const { properties } = this.props;

		switch (properties.show_spinner) {
			case true:
				return <Spinner size="large" />

			case false:
				return (
					<Card>
						<Text style={styles.title}>REGISTER NEW ACCOUNT</Text>
						<View>
							<CardItem>
								<Input
									onChange={value => this.setState({ email: value.trim() }, () => this.emailValid())}
									label="email address"
									name="email"
									value={this.state.email}
									placeholder="Enter your email"
									error={this.state.emailError} />
							</CardItem>
							<CardItem>
								<Input
									onChange={value => this.setState({ password: value.trim() }, () => this.passwordValid())}
									label="password"
									name="password"
									value={this.state.password}
									placeholder="Enter your password"
									error={this.state.passwordError}
									secure={true} />
							</CardItem>
							<CardItem>
								<Button onClick={this.register} disabled={this.state.disabled}   >Register</Button>
							</CardItem>
						</View>
					</Card>
				);
			default:
				return null
		}

	}



	render() {
		this.props.properties.showSpinner(false)
		this.props.properties.setError({})
		return (
			<View>
				{this.renderForm()}
			</View>
		);

	}

}