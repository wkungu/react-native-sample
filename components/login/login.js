import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { observer, inject } from 'mobx-react'
import validatorjs from 'validatorjs';
import lang from 'validatorjs/src/lang';
import { Form } from '../common'
import LoginForm from './form'

lang._set('en', {});

@inject('properties') @observer
export default class Login extends Component {

	render() {


		const fields = [
			{
				name: 'user_email',
				placeholder: 'Enter your email',
				label: 'email address',
				value: '',
				default: '',
				rules: 'required|email'
			},
			{
				name: 'user_password',
				placeholder: 'Enter your password',
				label: 'password',
				value: '',
				default: '',
				rules: 'required|string|between:3,20'
			}

		];


		const form = new Form({ fields });
		form.name = 'login-form';

		this.props.properties.showSpinner(false);
		this.props.properties.setError({});

		return (
			<View>
				<LoginForm form={form} />
			</View>
		)
	}

}