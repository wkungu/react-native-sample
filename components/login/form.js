import React, { Component } from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { observer, inject } from 'mobx-react'
import { Redirect } from 'react-router-native'
import { Card, CardItem, Input, Button, Spinner } from '../common'

const styles = StyleSheet.create({
    title: {
        justifyContent: 'center',
        alignItems: 'center',
        color: 'skyblue',
        fontWeight: '600',
        textAlign: 'center',
        textDecorationLine: 'underline',
        height: 40,
        paddingLeft: 5,
        paddingTop: 5,
        paddingBottom: 5,
    }
});


@inject('properties') @observer
export default class LoginForm extends Component {

    state = {
        email: '', password: ''
    }

    renderError = () => {

        let error = this.props.properties.error;

        let len = Object.keys(error).length;

        return (len > 0) ? <CardItem><Text style={{ color: 'red', fontSize: 10 }} >{error.title} {error.message}</Text></CardItem> : null;

    }


    renderForm = () => {

        const { form, properties } = this.props;

        switch (properties.show_spinner) {

            case true:
                return <Spinner size="large" />

            case false:
                return (
                    <Card>
                        <Text style={styles.title}>LOGIN</Text>
                        <View>
                            <CardItem>
                                <Input
                                    label={form.$('user_email').label}
                                    name={form.$('user_email').name}
                                    error={form.$('user_email').error}
                                    value={this.state.email}
                                    onChange={(value) => this.setState({ email: value }, () => form.$('user_email').value = this.state.email)}
                                    placeholder={form.$('user_email').placeholder} />
                            </CardItem>
                            <CardItem>
                                <Input
                                    label={form.$('user_password').label}
                                    name={form.$('user_password').name}
                                    error={form.$('user_password').error}
                                    value={this.state.password}
                                    onChange={(value) => this.setState({ password: value }, () => form.$('user_password').value = this.state.password)}
                                    secure
                                    placeholder={form.$('user_password').placeholder} />
                            </CardItem>
                            <CardItem>
                                <Button onClick={form.onSubmit} disabled={!form.isValid}  >Login</Button>
                            </CardItem>
                        </View>
                    </Card>
                )

            default:
                return null

        }

    }

    render() {

        const { properties } = this.props;

        return (
            <View>

                {(properties.logged_in) ? <Redirect to="/albums" /> : null}

                {this.renderError()}

                {this.renderForm()}


            </View>
        )
    }
}
