import React, { Component } from 'react';
import { AppRegistry } from 'react-native';
import Root from './components/root';

export default class Sample extends Component {
    render() {

        console.disableYellowBox = true;

        return <Root />
    }
}

AppRegistry.registerComponent('Sample', () => Sample);
